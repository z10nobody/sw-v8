FROM python:3.9
WORKDIR /appfront
COPY app/frontend/ /appfront
RUN pip3 install --upgrade pip -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["flask_test.py"]
