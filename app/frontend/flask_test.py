from flask import Flask, request,render_template
from itertools import groupby
import mysql.connector
import os
swdb = mysql.connector.connect(
  host=os.environ['server_name'],
  user=os.environ['user_name'],
  password=os.environ['user_pwd'],
  database=os.environ['db_name']
)

app = Flask(__name__)
@app.route("/")
def main():
    return "Welcome!"


@app.route('/db_test', methods=['GET'])
def db_test():
    mycursor = swdb.cursor()
    planet_name_cursor = swdb.cursor()
    planet_name_cursor.execute("SELECT planets.name, planets.gravity, planets.climate FROM planets group by planets.name, planets.gravity, planets.climate")
    pln = planet_name_cursor.fetchall()
    # res = []
    # for i in pln:
      
      
    #   mycursor.execute(f"SELECT planets.name, planets.gravity, planets.climate, persons.name as resident, persons.gender as gender, \
    #                  persons.homeworld as homeworld \
    #                  from persons inner join planets on persons.name = planets.residents where planets.name = '{i[0]}'")
    #   res = mycursor.fetchall()
    #   arr.extend(res)
    mycursor.execute(f"SELECT planets.name, planets.gravity, planets.climate, planets.residents as resident, persons.gender as gender, \
                     persons.homeworld as homeworld \
                         from persons JOIN planets ON persons.name = planets.residents ORDER BY planets.name;")
    res = mycursor.fetchall()
    mycursor.execute(f"SELECT name, gravity, climate, residents from planets where residents = 'n/a';")
    res1 = mycursor.fetchall()
    print(res1)
    res = res + res1
    # for todo in res:
    #   print(todo[0], ':', todo[1], todo[2], todo[3], todo[4])
    #print(res)
    lists = {}
    
    if len(res) != 0:
      for k, g in groupby(res, key=lambda t:t[0:3]):
          lists[k] = list(g)
          
    else:
        lists[k] = 'None'
    print(lists)
    return render_template('db_test.html', lists=lists)
    # for list_, items in lists.items():
    #   print('Planet name',list_[0], '\n', '\tPlanet gravity:', list_[1], '\n', '\tPlanet climate:', list_[2])
    #   #print(list_)
    #   for item in items:
    #     print('\tResident name:', item[3])
    # print(arr)
    # print(arr)
    # print(arr)
    # return render_template("db_test.html", res=res)
app.run(host="0.0.0.0")