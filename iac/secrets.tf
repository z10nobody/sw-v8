resource "kubernetes_secret" "rds_conn_secrets" {

  for_each = var.app_env

  metadata {
    name      = "rds-conn-secrets"
    namespace = kubernetes_namespace.k8s_ns[each.key].metadata.0.name
    labels = {
      "sensitive" = "true"
      "app"       = "${var.app_name}"
    }
  }

  data = {
    # string = base64encode("server=${aws_db_instance.swdb_mysql[each.key].address};database=${var.db-name};user=${var.db_username};password=${random_string.mysql_password[each.key].result}")
    server=aws_db_instance.swdb_mysql[each.key].address
    database=var.db-name
    user=var.db_username
    password=random_string.mysql_password[each.key].result
  }

  depends_on = [
    kubernetes_namespace.k8s_ns
  ]
}

data "kubernetes_secret" "gitlab_sa_secret" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account.default_secret_name
  }
}



