module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"
  cluster_name    = "sw-eks-cluster"
  cluster_version = "1.21"
  vpc_id = module.vpc.vpc_id
  subnets = module.vpc.private_subnets
  
  

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = "t2.small"
      # additional_userdata           = "kubectl config set-cluster k8s --server=\"$(terraform output cluster_endpoint | tr -d \\'\"\\')\" && kubectl config set clusters.k8s.certificate-authority-data $(terraform output cluster_cert_data | tr -d \\'\"\\')"
 # &&	kubectl apply -f k8s/config-manifest.yaml
      additional_security_group_ids = [aws_security_group.worker_group_mgmt.id]
      asg_desired_capacity          = 3
      asg_max_capacity = 3
      asg_min_capacity = 2
    }
  ]
  
}

resource "helm_release" "cert_manager" {
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  name       = "cert-manager"
  create_namespace = true
  namespace        = "certmgr"
  set {
    name  = "installCRDs"
    value = "true"
  }
  depends_on = [
    module.eks
  ]
}


resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  namespace        = "ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  version          = "4.0.16"
  repository       = "https://kubernetes.github.io/ingress-nginx"

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-backend-protocol"
    value = "tcp"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
    value = "true"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    value = "nlb"
  }

  depends_on = [
    helm_release.cert_manager
  ]
}

resource "helm_release" "prometheus" {
  chart      = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  name       = "kube-prometheus-stack"
  create_namespace = true
  namespace        = "monitoring"
  depends_on = [
    module.eks
  ]
}

resource "null_resource" "k8s_initial_setup" {
	provisioner "local-exec" {
		command  =  "aws eks --region eu-central-1 update-kubeconfig --name sw-eks-cluster" # &&	kubectl apply -f k8s/config-manifest.yaml"
	}
	depends_on = [module.eks]
}

