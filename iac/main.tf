terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
      }
  }
}

provider "aws" {
  region = var.aws_region
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.cluster.token
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}


#VPC

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.2.0"

  name                 = "sw_vpc"
  cidr                 = var.vpc_cidr_block
  azs                  = var.az_list
  private_subnets      = var.private_subnet_cidr_block
  public_subnets       = var.public_subnet_cidr_block
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
  map_public_ip_on_launch = true

  public_subnet_tags = {
    "kubernetes.io/cluster/sw-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/sw-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_db_subnet_group" "rds-subnet" {
  name       = "rds-subnet"
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name = "rds-subnet"
  }
}

#RDS

resource "random_string" "mysql_password" {
  for_each = var.app_env
  length   = 14
  special  = false
}

resource "aws_db_parameter_group" "rds_params" {
  name   = "rds-params"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_instance" "swdb_mysql" {
  for_each = var.app_env
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  port                   = "3306"
  identifier             = "mysql-${each.key}"
  identifier_prefix      = null
  multi_az               = false
#   storage_encrypted      = false
  skip_final_snapshot    = true
  snapshot_identifier    = null
  username               = var.db_username
  password               = random_string.mysql_password[each.key].result
  name                   = var.db-name
  parameter_group_name   = aws_db_parameter_group.rds_params.name
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet.id
  vpc_security_group_ids = ["${aws_security_group.rds-sg.id}"]

  tags =  merge (var.tags, {Name = "sw_db-mysql-${each.key}"})
}

resource "kubernetes_cluster_role" "cluster-deployer" {
  metadata {
    name = "cluster-deployer"
  }

  rule {
    api_groups = ["", "apps", "networking.k8s.io", "keda.sh"]
    resources  = ["pods", "deployments", "services", "ingresses", "secrets", "scaledobjects"]
    verbs      = ["get", "list", "create", "update", "patch"]
  }

  rule {
    api_groups = ["", "autoscaling"]
    resources  = ["namespaces", "horizontalpodautoscalers"]
    verbs      = ["get", "list"]
  }
}


resource "kubernetes_service_account" "gitlab_service_account" {
  metadata {
    name = "gitlab-service-account"
  }
}


resource "kubernetes_cluster_role_binding" "gitlab_role_binding" {
  metadata {
    name = "gitlab_role_binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account"
  }
}



  
