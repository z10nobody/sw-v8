variable "aws_region" {
  description = "AWS Region"
  default = "eu-central-1"
  type = string
}

variable "az_list" {
  description = "Available zones in region"
  default     = ["eu-central-1a", "eu-central-1b"]
  type        = list(any)
}


variable "tags" {
  type = map
  default = {
      Owner = "Sergei Suntsov"
      Project = "SW version 8"
      Email = "z10-nobody@mail.ru"
  }
}

# VPC Variables

variable "vpc_cidr_block" {
  description = "VPC cidr block"
  default = "192.168.0.0/21"
}

variable "public_subnet_cidr_block" {
  description = "cidr block for public subnet a"
  default = ["192.168.0.0/24", "192.168.1.0/24"]
  type = list
}

variable "private_subnet_cidr_block" {
  description = "cidr block for private subnet a"
  default = ["192.168.6.0/24", "192.168.7.0/24"]
  type = list
}

# Instance variaables

variable "instance_type" {
  default = "t2.small"
}

# App variables

variable "app_env" {
  type    = set(string)
  default = ["prod", "dev"]
}

variable "app_name" {
  type = string
  default = "sw-v8"
}


# DB variables

variable "db_username" {
  type = string
  default = "z10"
}

variable "db-name" {
  type = string
  default = "sw_db"
}