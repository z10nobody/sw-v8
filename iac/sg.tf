resource "aws_security_group" "ssh-access" {
  name_prefix = "ssh-access_sg"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = var.private_subnet_cidr_block
  }
  tags = {
    Name = "ssh-access_sg"
  }
}

resource "aws_security_group" "rds-sg" {
  name   = "RDS-SG"
  vpc_id = module.vpc.vpc_id
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks = var.private_subnet_cidr_block
    # security_groups = [aws_security_group.ssh-access.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.private_subnet_cidr_block
  }

  tags = {
    Name = "RDS-SG"
  }
}

resource "aws_security_group" "worker_group_mgmt" {
  name_prefix = "worker_group_mgmt"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = var.private_subnet_cidr_block
  }
}
