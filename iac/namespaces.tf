resource "kubernetes_namespace" "k8s_ns" {

  for_each = var.app_env

  metadata {
    annotations = {
      name = each.key
    }

    labels = {
      mylabel = each.key
    }

    name = each.key
  }
}

