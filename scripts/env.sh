if [ "${CI_COMMIT_BRANCH}" == "main" ]; then
	export envtype=prod
	export tagName=$CI_COMMIT_SHORT_SHA
else
	export envtype=dev
	export tagName=$CI_COMMIT_SHORT_SHA
fi
